FROM node:10
MAINTAINER pleddy

RUN apt-get update
RUN apt-get install 'tesseract*' -y
RUN ln -s /usr/bin/tesseract /usr/local/bin/tesseract

WORKDIR /usr/src/app

COPY package.json package*.json ./

RUN npm install --only=production

COPY . .

CMD [ "npm", "start" ]
