"use strict";

var express = require('express');
var router = express.Router();
var readChunk = require('read-chunk'); 
var fileType = require('file-type');
var multer = require('multer');
var child_process = require('child_process');
var fs = require('fs');
var exec = require('child_process').execFile,
    child;

/* GET home page. */
router.get('/', function(req, res, next) {
  if (typeof variable !== 'undefined') {
    console.log('M_ALERT: ', m_alert) 
  }
  res.render('index', { title: 'Express' });
});

router.post('/', multer({dest:'./uploads/',  limits: {fileSize: 5 * 1000000 }}).single('m_image'), function (req, res) {
  console.log(req.body.lang)
  console.log(req.file) 
  //console.log(req.body.orientation)

  var m_buffer = readChunk.sync(req.file.path, 0, 262);
  var m_fileext = fileType(m_buffer)
  if (m_fileext === null ) { m_fileext = { ext: 'unknown' } }
  console.log(m_fileext)
  if (m_fileext.ext.match(/^(jpg|png)$/g)) {

    var m_tesseract_opts = [req.file.path, 'stdout', '-l', req.body.lang]
    if ( req.body.orientation == "Vertical" ) { m_tesseract_opts.push('-psm', 5) }
    console.log('Tesseract opts: ', m_tesseract_opts)

    console.log('Got image type file')
    var m_lang = 'spa'
    child = exec('/usr/local/bin/tesseract', m_tesseract_opts,
      function (error, stdout, stderr) {
        //console.log('stdout: ' + stdout);
        console.log('stderr: ' + stderr);
        if (error !== null) {
          console.log('exec error: ' + error);
        }
        fs.unlinkSync(req.file.path);
        console.log('successfully deleted '  + req.file.path);
        var m_ocrd = stdout.replace(/\n\n/g, '<p>');
        m_ocrd = m_ocrd.replace(/\n/g, ' ');
        m_ocrd = m_ocrd.replace(/<p>/g, "\n\n");
        m_ocrd = m_ocrd.replace(/(\- |\— )/g, '');
        res.render('index', { title: 'Express', m_buff: m_fileext, m_ocrd: m_ocrd, m_lang: req.body.lang });
    });
  } else {
    fs.unlinkSync(req.file.path);
    console.log('successfully deleted '  + req.file.path);
    res.render('index', { title: 'Express', m_buff: m_fileext });
  }

//  res.render('index', { title: 'Express', m_buff: m_fileext, m_ocrd: m_result['stdout'].toString() });
})

router.use(function (err, req, res, next) {
  console.log('Error: ', err)
  if ( err.code == 'LIMIT_FILE_SIZE' ) {
    var m_alert = { status: true, message: 'File too big, try smaller' }
  }
  res.render('index', { title: 'Express', m_alert: m_alert });
  //res.status(413).send('File too large');
})

module.exports = router;

